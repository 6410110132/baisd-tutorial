times = 5
f0, f1, f2 = 0, 1, 1
counter = 0
ans = [0]
for i in range(2, times+2):
    ans.append(f2)
    f2 = f0 + f1
    f0 = f1
    f1 = f2
    counter += 1

print(ans, end="")
