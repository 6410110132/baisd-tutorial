def fibonacci(num):
    if num <= 1:
        return num
    else:
        return(fibonacci(num-1) + fibonacci(num-2))


ans = []

fn = (int(input('Input number of Fibonacci number: '))+1)
for i in range(fn):
    ans.append(str(fibonacci(i)))

print(", ".join(ans))
