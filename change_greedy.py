def change(money):
    coin = []
    coin.append(money // 10)
    money = money % 10
    coin.append(money // 5)
    money = money % 5
    coin.append(money // 4)
    money = money % 4
    coin.append(money // 2)
    money = money % 2
    coin.append(money // 1)
    return coin


if __name__ == "__main__":
    money = 25
    ch = change(money)
    print('coin 10:', ch[0])
    print('coin  5:', ch[1])
    print('coin  4:', ch[2])
    print('coin  2:', ch[3])
    print('coin  1:', ch[4])
